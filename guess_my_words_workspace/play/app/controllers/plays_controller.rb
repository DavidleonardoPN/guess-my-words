class PlaysController < ApplicationController

  def index
    @all_words = ["Buku", "Roti", "Kartu" , "Makan"]

    if params[:play].present?
    
      @check = params[:play][:guess] == params[:play][:keyword].downcase
    
      if @check
        flash[:success] = "That's correct"
      else
        flash[:danger] = "That's Wrong"
      end
    end

  end

end
